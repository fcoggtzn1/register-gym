// Create a Form widget.
import 'package:flutter/material.dart';

class RegisterUser extends StatefulWidget {
  @override
  RegisterUserFormState createState() {
    return RegisterUserFormState();
  }
}
// Create a corresponding State class. This class holds data related to the form.
class RegisterUserFormState extends State<RegisterUser> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            decoration: const InputDecoration(
              icon: const Icon(Icons.person),
              hintText: 'Enter your name',
              labelText: 'Name',

            ),
            validator: (value) {
              if (value!.isEmpty) {
                return 'Please enter the name';
              }
              return null;
            },
          ),
          TextFormField(
            decoration: const InputDecoration(
              icon: const Icon(Icons.phone),
              hintText: 'Enter a phone number',
              labelText: 'Phone',
            ),
            validator: (value) {
              if (value!.isEmpty) {
                return 'Please a valid phone ';
              }
              return null;
            },
          ),
          TextFormField(
            decoration: const InputDecoration(
              icon: const Icon(Icons.calendar_today),
              hintText: 'Enter your date of birth',
              labelText: 'Dob',

            ),
            validator: (value) {
              if (value!.isEmpty) {
                return 'Please enter the valid Dob';
              }
              return null;
            },
          ),
          Container(
              padding: const EdgeInsets.only(left: 150.0, top: 40.0),
              child: new RaisedButton(
                child: const Text('Submit'),
                onPressed: (){   // It returns true if the form is valid, otherwise returns false
                  if (_formKey.currentState!.validate()) {

                    // If the form is valid, display a Snackbar.
                    Scaffold.of(context)
                        .showSnackBar(SnackBar(content: Text('Data is in processing.')));
                    Navigator.pushNamed(context,"/enter");
                  }  },
              )),
        ],
      ),
    );
  }
}