// Create a Form widget.
import 'package:flutter/material.dart';

class EnterUser extends StatefulWidget {
  @override
  _enterUserFormState createState() {
    return _enterUserFormState();
  }
}
// Create a corresponding State class. This class holds data related to the form.
class _enterUserFormState extends State<EnterUser> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            decoration: const InputDecoration(
              icon: const Icon(Icons.door_front_door),
              hintText: 'Enter your name/ID',
              labelText: 'Name',

            ),
            validator: (value) {
              if (value!.isEmpty) {
                return 'Please enter the name';
              }
              return null;
            },
          ),

          Container(
              padding: const EdgeInsets.only(left: 150.0, top: 40.0),
              child: new RaisedButton(
                child: const Text('Submit'),
                onPressed: (){   // It returns true if the form is valid, otherwise returns false
                  if (_formKey.currentState!.validate()) {
                    // If the form is valid, display a Snackbar.
                    Scaffold.of(context)
                        .showSnackBar(SnackBar(content: Text('Data is in processing.')));
                    Navigator.pop(context);
                  }  },
              )),
        ],
      ),
    );
  }
}